from sklearn.metrics import mean_squared_error, r2_score
from sklearn import linear_model
from sklearn import preprocessing
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

path = "datasets/dataset_train.csv"
df = pd.read_csv(path)

all_cols = ['Index', 'Hogwarts House', 'First Name', 'Last Name', 'Birthday',
			'Best Hand', 'Arithmancy', 'Astronomy', 'Herbology',
			'Defense Against the Dark Arts', 'Divination', 'Muggle Studies',
			'Ancient Runes', 'History of Magic', 'Transfiguration', 'Potions',
			'Care of Magical Creatures', 'Charms', 'Flying']
classes_cols = ['Arithmancy', 'Astronomy', 'Herbology',
				'Defense Against the Dark Arts', 'Divination', 'Muggle Studies',
				'Ancient Runes', 'History of Magic', 'Transfiguration', 'Potions',
				'Care of Magical Creatures', 'Charms', 'Flying']
target_col = 'Hogwarts House'

col_len = max([len(x) for x in classes_cols])

df_cleaned = df.dropna()


mi = -10000000000
ma = 10000000000

R2Ss = []
COVs = []
MSEs = []
PEARSONS = []
for i in range(len(classes_cols)):
	R2S = []
	MSE = []
	COV = []
	PEARSON = []
	for ii in range(len(classes_cols)):
		x = df_cleaned[classes_cols[i]].to_numpy().reshape(-1, 1)
		y = df_cleaned[classes_cols[ii]].to_numpy().reshape(-1, 1)

		# x = preprocessing.MinMaxScaler().fit_transform(x)
		# y = preprocessing.MinMaxScaler().fit_transform(y)

		res = linear_model.LinearRegression()
		res.fit(x, y)
		y_ = res.predict(y)
		if classes_cols[i] == classes_cols[ii]:
			R2S.append(mi)
			MSE.append(ma)
			COV.append(mi)
			PEARSON.append(ma)
		else:
			R2S.append(r2_score(y, y_))
			MSE.append(mean_squared_error(y, y_))
			tmp = np.cov(x, y, rowvar=0)
			tmp_cov = (tmp[0][1] + tmp[1][0]) / 2
			COV.append(tmp_cov)
			nn = tmp_cov / (np.std(x) * np.std(y))
			PEARSON.append(nn)
	if R2S != []:
		R2Ss.append(R2S)
		MSEs.append(MSE)
		COVs.append(COV)
		PEARSONS.append(PEARSON)


R2Ss = np.array(R2Ss, np.float32)
MSEs = np.array(MSEs, np.float32)
COVs = np.array(COVs, np.float32)
PEARSONS = np.array(PEARSONS, np.float32)

def min_index(x):
	return np.unravel_index(x.argmin(), x.shape)


def max_index(x):
	return np.unravel_index(x.argmax(), x.shape)


def print_best_and_remove(MSEs, R2Ss, COVs, PEARSONS):
	idx_mse = min_index(MSEs)
	idx_r2s = max_index(R2Ss)
	idx_cov = max_index(COVs)
	idx_prs = min_index(PEARSONS)

	fig, axes = plt.subplots(nrows=1, ncols=4, figsize=(30, 5))
	fig.suptitle('Wesh wesh les amis, il va falloir reviser')
	sns.scatterplot(data=df_cleaned, x=classes_cols[idx_r2s[0]],
					y=classes_cols[idx_r2s[1]], hue=target_col, ax=axes[0])
	axes[0].set_title(f"R2 Score Max {R2Ss[idx_r2s[0]][idx_r2s[1]]}")
	sns.scatterplot(data=df_cleaned, x=classes_cols[idx_mse[0]],
					y=classes_cols[idx_mse[1]], hue=target_col, ax=axes[1])
	axes[1].set_title(f"MSE Min {MSEs[idx_mse[0]][idx_mse[1]]}")
	sns.scatterplot(data=df_cleaned, x=classes_cols[idx_cov[0]],
					y=classes_cols[idx_cov[1]], hue=target_col, ax=axes[2])
	axes[2].set_title(f"Covariance Max {COVs[idx_cov[0]][idx_cov[1]]}")
	sns.scatterplot(data=df_cleaned, x=classes_cols[idx_prs[0]],
					y=classes_cols[idx_prs[1]], hue=target_col, ax=axes[3])
	axes[3].set_title(f"Pearson Min {PEARSONS[idx_prs[0]][idx_prs[1]]}")
	plt.show()

	R2Ss[idx_r2s[0]][idx_r2s[1]] = mi
	R2Ss[idx_r2s[1]][idx_r2s[0]] = mi
	MSEs[idx_mse[0]][idx_mse[1]] = ma
	MSEs[idx_mse[1]][idx_mse[0]] = ma
	COVs[idx_cov[0]][idx_cov[1]] = mi
	COVs[idx_cov[1]][idx_cov[0]] = mi
	PEARSONS[idx_prs[0]][idx_prs[1]] = ma
	PEARSONS[idx_prs[1]][idx_prs[0]] = ma
	plt.show()


for i in range(1):
	print(f"{i} best: ")
	print_best_and_remove(MSEs, R2Ss, COVs, PEARSONS)

sns.scatterplot(data=df_cleaned, x="Astronomy",
				y="Defense Against the Dark Arts", hue=target_col)
plt.show()
