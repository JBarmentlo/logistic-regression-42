from sklearn import preprocessing
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

path = "datasets/dataset_train.csv"
df = pd.read_csv(path)

all_cols = ['Index', 'Hogwarts House', 'First Name', 'Last Name', 'Birthday',
            'Best Hand', 'Arithmancy', 'Astronomy', 'Herbology',
            'Defense Against the Dark Arts', 'Divination', 'Muggle Studies',
            'Ancient Runes', 'History of Magic', 'Transfiguration', 'Potions',
            'Care of Magical Creatures', 'Charms', 'Flying']
classes_cols = ['Arithmancy', 'Astronomy', 'Herbology',
                'Defense Against the Dark Arts', 'Divination', 'Muggle Studies',
                'Ancient Runes', 'History of Magic', 'Transfiguration', 'Potions',
                'Care of Magical Creatures', 'Charms', 'Flying']
target_col = 'Hogwarts House'

col_len = max([len(x) for x in classes_cols])

df_cleaned = df.dropna()


fig, axes = plt.subplots(nrows=3, ncols=5, figsize=(40, 20))
fig.suptitle('Wesh wesh les amis, il va falloir reviser')

for i in range(len(classes_cols)):
    g = sns.histplot(
        df_cleaned, x=classes_cols[i], hue=target_col, ax=axes[i // 5][i % 5])

plt.show()


std = []
std_nz = []
for i in range(len(classes_cols)):

    std.append(np.std(df_cleaned[classes_cols[i]]))
    data = df_cleaned[classes_cols[i]].to_numpy().reshape(-1, 1)
    data = preprocessing.MinMaxScaler().fit_transform(data)
    std_nz.append(np.std(data))

print()
print()
print()
for c, s, z in zip(classes_cols, std, std_nz):
    print(f"{c:{col_len}} std is {s:10.7} or normalized {z:10.7}")
print()
print()
print(
    f"The most homogenous class            is {classes_cols[std.index(min(std))]:{col_len}} with a std of {min(std):10.3}")
print(
    f"The most homogenous class normalized is {classes_cols[std_nz.index(min(std_nz))]:{col_len}} with a std of {min(std_nz):10.3}")
