from sklearn.metrics import mean_squared_error, r2_score
from sklearn import linear_model
from sklearn import preprocessing
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

path = "datasets/dataset_train.csv"
df = pd.read_csv(path)

all_cols = ['Index', 'Hogwarts House', 'First Name', 'Last Name', 'Birthday',
            'Best Hand', 'Arithmancy', 'Astronomy', 'Herbology',
            'Defense Against the Dark Arts', 'Divination', 'Muggle Studies',
            'Ancient Runes', 'History of Magic', 'Transfiguration', 'Potions',
            'Care of Magical Creatures', 'Charms', 'Flying']
classes_cols = ['Arithmancy', 'Astronomy', 'Herbology',
                'Defense Against the Dark Arts', 'Divination', 'Muggle Studies',
                'Ancient Runes', 'History of Magic', 'Transfiguration', 'Potions',
                'Care of Magical Creatures', 'Charms', 'Flying']
target_col = 'Hogwarts House'

col_len = max([len(x) for x in classes_cols])

df_cleaned = df.dropna()

sns.pairplot(df_cleaned, hue=target_col)
plt.show()
