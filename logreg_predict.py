from shaman_disciple import ShamanDisciple
from dataset import Dataset
import argparse

if __name__  == "__main__":
    parser = argparse.ArgumentParser(description='A matrix powered multinomial logistic regression')
    parser.add_argument("dataset_path", nargs='?', default="datasets/dataset_test.csv", help="path to the dataset to evaluate")
    parser.add_argument("thetas_path", nargs='?', default="thetas.csv", help="path to thetas file, in numpy format")
    parser.add_argument("--activation", default="softmax", help="name of activation function, defaults to softmax", action = 'store', choices=['sigmoid', 'softmax'])
    args = parser.parse_args()
    test_dataset = Dataset(args.dataset_path, standardize=False, test_set=True)
    shaman = ShamanDisciple(args.thetas_path, activation = args.activation)
    shaman.make_output_csv(test_dataset.x)