
from datetime import datetime
from numpy import matmul, transpose
import numpy as np
from dataset import *
from scipy.special import softmax

# float_formatter = "{:.2E}".format
# np.set_printoptions(formatter={'float_kind':float_formatter})
float_formatter = "{:.2f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

def normalize(a):
    col_sums = a.sum(axis=0)
    new_matrix = a / col_sums[np.newaxis, :]
    return new_matrix

def sigmoid(x):
    return 1/(1 + np.exp(-x))


def sofmax_row(x):
    return (softmax(x, axis = 1))


class Shaman():
    def __init__(self, dataset, time_limit = 1, activation = 'sigmoid', batch_size = 0):
        # * activation = 'sigmoid' or 'softmax'
        self.activation = self.get_activation_function(activation)
        self.p = dataset.p
        self.dataset = dataset
        self.thetas = np.zeros([dataset.y_p, self.p + 1], dtype = float)
        self.old_thetas = self.thetas
        self.lr = 0.1
        self.lr_decay = 1.0 / 2
        self.oldcost = 0.0
        self.c = 1.0 / 2
        self.lr_increase = 2
        self.start_time = None
        self.time_limit = time_limit
        self.standardize = dataset.standardized
        if batch_size == 0:
            self.batch_size = self.dataset.x.shape[0]
        else:
            self.batch_size = batch_size


    def get_activation_function(self, activation):
        if activation == 'sigmoid':
            return sigmoid
        if activation == 'softmax':
            return sofmax_row
        print("You have entered an incorrect activation function name, defaulting to softmax")
        return sofmax_row


    def time_stop(self):
        return ((datetime.now() - self.start_time).seconds >= self.time_limit)


    def predict(self, data, thetas = None):
        """
            Compute prediction for given data.

            Args:
                data (np.ndarray): data to use as input
                thetas (np.ndarray, optional): Replace the thetas with your custom ones.

            Returns:
                np.ndarray: predicted outcome y = sigmoid(data * transpose(thetas)).
        """        
        if thetas is None:
            thetas = self.thetas
        if (data.ndim == 1):
            data = data[np.newaxis, :]
        out = matmul(data, transpose(self.thetas))
        return (self.activation(out))


    def error(self, thetas = None)->np.ndarray:
        """
            Compute the error matrix for the dataset.

            Args:
                thetas (ndarray, optional): Thetas to use for prediction. Defaults to the current thetas.

            Returns:
                error (np.ndarray): the error matrix = x_pred - y
        """        
        if thetas is None:
            thetas = self.thetas
        predictions = self.predict(self.dataset.x, thetas)
        error = predictions - self.dataset.y
        return (error)


    def mean_squared_error(self, thetas = None):
        if thetas is None:
            thetas = self.thetas   
        squared_error = np.square(self.error(thetas))
        self.error()
        return (np.mean(squared_error) / 2)


    def compute_gradients(self):
        x_pred = self.predict(self.dataset.x)
        error = x_pred - self.dataset.y
        gradients = matmul(transpose(self.dataset.x), error) / len(error)
        return (transpose(gradients))


    def compute_stochastic_gradients(self, batch_size):
        datasize = self.dataset.x.shape[0]
        if (batch_size >= datasize):
            batch_size = datasize
        mask = np.random.choice(datasize, batch_size, replace=False)
        x_pred = self.predict(self.dataset.x[mask, :])
        error = x_pred - self.dataset.y[mask, :]
        gradients = matmul(transpose(self.dataset.x[mask]), error) / len(error)
        return (transpose(gradients))


    def update_thetas(self):
        self.old_thetas = self.thetas
        gradients = self.compute_stochastic_gradients(batch_size=self.batch_size)
        self.thetas = self.thetas - (self.lr * gradients)

    
    def update_costs(self):
        tmpold = self.oldcost
        self.oldcost = self.newcost
        self.newcost = self.mean_squared_error()
        return tmpold


    def undo_update_costs(self, tmpold):
        self.newcost =self.oldcost
        self.oldcost = tmpold


    def training_loop(self):
        self.start_time = datetime.now()
        self.newcost = self.mean_squared_error()
        while (not self.time_stop()):
            tmpold = self.oldcost
            self.update_thetas()
            tmpold = self.update_costs()
            if (self.newcost > self.oldcost):
                self.lr = self.lr * self.lr_decay
                self.thetas = self.old_thetas
                self.undo_update_costs(tmpold)


    def unstandardize_thetas(self):
        at = self.thetas[:, 1:]
        pt = self.thetas[:, 1]
        scaler = self.dataset.x_scaler
        at = at / scaler.scale_
        pt = pt - np.dot(at, scaler.mean_)
        self.thetas = np.concatenate((pt[:, np.newaxis], at), axis = 1)


    def write_thetas_to_file(self, filename="thetas.csv"):
        if self.standardize:
            self.unstandardize_thetas()
        np.savetxt(filename, self.thetas, delimiter = ',')


    def __str__(self):
        return (f"Cost: {self.newcost}, Thetas: {self.thetas}, LR {self.lr:4.2E}")


def add_complementary_column(a):
    b = np.ones([len(a)]) - np.sum(a, axis=1)
    full = np.concatenate((a, b[:, np.newaxis]), axis = 1)
    return (full)

def evaluate_precision(shaman, x, y):
    x_pred = shaman.predict(x)
    errors = 0
    for i in range(len(y)):
        if (np.argmax(x_pred[i, :]) != np.argmax(y[i, :])):
            errors += 1
    return (1.0 - errors / len(y))