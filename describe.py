import pandas as pd
import argparse
import sys
import math

PURPLE = '\033[95m'
BLUE = '\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
RESET = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'

def get_percentile(data: list, percent: int):
    data.sort()
    float_index = len(data) * (percent / 100)
    index = int(float_index)
    surplus = float_index - index
    if index >= len(data):
        index = len(data) - 1
    left = data[index]
    if len(data) > index + 1:
        right = data[index + 1]
        result = left * (1 - surplus) + (right * (surplus))
    else:
        result = left
    return float(result)

def get_median(data: list):
    data.sort()
    return get_percentile(data, 50)

def get_mean(data: list):
    mean = sum(data) / len(data)
    return mean

def get_var(data: list, n=2, r=0):
    mean = get_mean(data)
    variance = sum([abs(x - mean) ** n for x in data]) / (len(data) - r)
    return variance

def get_std(data: list, s=1/2, e=2, r=0):
    return get_var(data, e, r) ** s

def manage_nan(data: list, method: str ="REMOVE"):
    if method == "REMOVE":
        data = list(filter(lambda x: not math.isnan(x), data))
        return data
    r_nan_data = manage_nan(data, "REMOVE")
    if type(r_nan_data) == type(None) or len(r_nan_data) == 0:
        return None
    if method == "MEDIAN":
        median = get_median(r_nan_data)
        return list(map(lambda x: median if math.isnan(x) else x, data))
    elif method == "MEAN":
        mean = get_mean(r_nan_data)
        return list(map(lambda x: mean if math.isnan(x) else x, data))
    elif method == "SEXY":
        return list(map(lambda x: 69 if math.isnan(x) else x, data))
    return None

def check_data(data: list, nan_method: str):
    if type(data[0]) != type(0.0) and type(data[0]) != type(0):
        return None
    data = manage_nan(data, nan_method)
    if data == None:
        return None
    return data

def describe_feature(data: list, centiles: list):
    results = [float(len(data)),
				get_mean(data),
				get_median(data),
				get_std(data),
				get_var(data, n=3) / (get_std(data) ** 3),
				get_var(data, n=4) / get_std(data, s=4/2)]
    for c in centiles:
        r = get_percentile(data, c)
        results.append(r)
    return results

def get_indexes(centiles:list):
    indexes = ["count", "mean", "median", "std", "skewness", "kurtosis"]
    for c in centiles:
        if c == 0:
            c = "min"
        elif c == 100:
            c = "max"
        else:
            c = str(c) + "%"
        indexes.append(c)
    return indexes

def longest_string(x: list):
    return max([len(f"{i:.7}") + 1 for i in x])

def print_describe(results:dict, indexes: list, black: bool):
    if black:
        CLR_COL = ""
        CLR_IDX = ""
        CLR_OUT = ""
        END = ""
    else:
        CLR_COL = YELLOW
        CLR_IDX = RED
        CLR_OUT = BLUE
        END = RESET
    lens = []
    lens.append(longest_string(indexes))
    for k in results.keys():
        mx = longest_string(results[k])
        if mx < len(k):
            mx = len(k)
        lens.append(mx)
    print(f"{' ':{lens[0]}}", end="")
    for k, l in zip(results.keys(), lens[1:]):
        print(f" {CLR_OUT}|{END} {CLR_COL}{k:<{l}}{END}", end="")
    print()
    print(f"{' ':{lens[0]}}{CLR_OUT}", end="")
    for k, l in zip(results.keys(), lens[1:]):
        print(f" | {'-' * l}", end="")
    print(END)
    desc_val = 0
    for idx in indexes:
        print(f"{CLR_IDX}{idx:<{lens[0]}}{END}", end="")
        i = 0
        for feat in results.keys():
            print(
                f" {CLR_OUT}|{END} {results[feat][desc_val]:>{lens[1 + i]}.5}", end="")
            i += 1
        desc_val += 1
        print()



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", help="Path to dataset to describe")
    parser.add_argument("-c", "--centile", help="Adds centile to data description", type=int, nargs="+")
    parser.add_argument("-b", "--black", help="Black and white print", action='store_true', default=False)
    parser.add_argument("-n", "--nan", choices=['REMOVE', 'MEAN', 'MEDIAN', 'SEXY'], help="Nan management", default="MEDIAN")
    args = parser.parse_args()


    centiles = [0, 25, 50, 75, 100]
    if args.centile:
        centiles.extend(args.centile)
        centiles = list(set(centiles))
        centiles.sort()
        if centiles[0] < 0 or centiles[-1] > 100:
            print("Error: centiles should be between 0 and 100")
            sys.exit(0)
    data = pd.read_csv(args.dataset).to_dict()
    BIGGEST_NAME = max([len(s) for s in data.keys()]) + 1
    results = {}
    for k, v in data.items():
        if k == "Index":
            continue
        feature = list(v.values())
        feature = check_data(feature, args.nan)
        if feature != None:
            res = describe_feature(feature, centiles)
            results[k] = res
    indexes = get_indexes(centiles)
    print_describe(results, indexes, args.black)
