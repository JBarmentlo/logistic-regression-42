
from datetime import datetime
from numpy import matmul, transpose
import numpy as np
from dataset import *
from scipy.special import softmax
from shaman import sofmax_row, sigmoid
# float_formatter = "{:.2E}".format
# np.set_printoptions(formatter={'float_kind':float_formatter})
float_formatter = "{:.2f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})

def normalize(a):
    col_sums = a.sum(axis=0)
    new_matrix = a / col_sums[np.newaxis, :]
    return new_matrix


class ShamanDisciple():
    def __init__(self, thetas_path, activation = "softmax"):
        self.thetas = np.genfromtxt(thetas_path, delimiter=',')
        self.activation = self.get_activation_function(activation)

    def get_activation_function(self, activation):
        if activation == 'sigmoid':
            return sigmoid
        if activation == 'softmax':
            return sofmax_row
        print("You have entered an incorrect activation function name, defaulting to softmax")
        return sofmax_row


    def predict(self, data):
        """
            Compute prediction for given data.

            Args:
                data (np.ndarray): data to use as input
                thetas (np.ndarray, optional): Replace the thetas with your custom ones.

            Returns:
                np.ndarray: predicted outcome y = sigmoid(data * transpose(thetas)).
        """        
        if (data.ndim == 1):
            data = data[np.newaxis, :]
        out = matmul(data, transpose(self.thetas))
        return (self.activation(out))


    def make_output_csv(self, data):
        pred = self.predict(data)
        df = pd.DataFrame()
        df["Index"] = [x for x in range(len(pred))]
        houses = ["Gryffindor", "Hufflepuff", "Ravenclaw", "Slytherin"]
        df["Hogwarts House"] = np.argmax(pred, axis=1)
        df["Hogwarts House"] = df["Hogwarts House"].apply(lambda x: houses[x])
        df.to_csv("houses.csv", index=False)

