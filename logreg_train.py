import argparse
from datetime import datetime
from dataset import *
from shaman import Shaman, evaluate_precision

def read_thetas(self, path = 'thetas.csv'):
    self.thetas = np.genfromtxt(path, delimiter=',')

if __name__  == "__main__":
    parser = argparse.ArgumentParser(description='A typical logistic regression')
    parser.add_argument("data_path", nargs = '?', default="datasets/dataset_train.csv", help="path to input data")
    parser.add_argument("--activation", default="softmax", help="name of activation function", action = 'store', choices=['sigmoid', 'softmax'])
    parser.add_argument("--time", default=2, help="time limit for learning (seconds), defaults to 2.", action = 'store', nargs = "?", type=float)
    parser.add_argument("--batchsize", default=0, help="batch size, 0 = all data", action = 'store', nargs = "?", type=int)
    parser.add_argument("--no_standardize", help='do not standardize training data', action='store_false')
    args = parser.parse_args()
    d = Dataset(args.data_path, standardize = args.no_standardize)
    shaman = Shaman(d, time_limit = args.time, activation=args.activation, batch_size=args.batchsize)
    shaman.training_loop()
    shaman.write_thetas_to_file()
    d.destandardize()